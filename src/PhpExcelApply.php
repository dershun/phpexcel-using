<?php 
namespace Dershun\PhpexcelUsing;
use PHPExcel_IOFactory;
use PHPExcel;

/**
* PHPExcel应用类
* 需先安装PHPExcel扩展  composer require phpoffice/phpexcel
*/
class PhpExcelApply
{   
    //导出Excal表格配置
    //   writer_type Excel版本选择 Excel2007/Excel5
    //   file_name 文件名称
    //   title 工作表名
    //   data_start 起始行
    //   start_column 数据起始列
    //   field_matching 是否开启字段匹配，false：按顺序写入数组数据；true  根据Head设置的name值匹配写入数据
    //
    private $Config = [
        "writer_type"    => "Excel2007",
        "file_name"      => "数据表格",
        "title"          => "",
        "data_start"     => 2,
        "start_column"   => 1,
        "field_matching" => false,
    ];

    //表头设置
    private $Head = [];

    public function __construct()
    {   
        $this->PHPExcel = new PHPExcel();
        $this->excelSheet = $this->PHPExcel->getActiveSheet();
    }

    /**
     * 设置导出Excal表格配置
     * @param array $Config [description]
     */
    public function setExportConfig(array $Config)
    {
        if(empty($Config)) return $this;

        foreach ($Config as $key => $value) {
            $this->Config[$key] = $value;
        }

        return $this;
    }

    /**
     * 设置Excel表头
     * @param array $Head 表头数组
     * [
     *     "A1"=>[
     *         "title"      => "标题",
     *         "name"       => "字段名（设置field_matching为true时有效）",
     *         "width"      => "列宽（int）",
     *         "height"     => "行高（int）",
     *         "horizontal" => "水平对齐方式(center|left|right)"
     *         "vertical"   => "垂直对齐方式(center|top|bottom)"
     *         "merge"      => "合并单元格（A2）"
     *     ]
     * ]
     */
    public function setExportHead(array $Head){
        if(empty($Head)) return $this;

        foreach ($Head as $key => $value) {
            $column = preg_replace('/\d+/',"",$key);
            $this->excelSheet->setCellValue($key, $value['title'])
                ->getColumnDimension($column)
                ->setWidth($value['width']??-1);

            //设置行高
            if(isset($value['height'])){
                $dimension = preg_replace('/[a-zA-Z]+/',"",$key);
                $this->excelSheet->getRowDimension($dimension)->setRowHeight($value['height']);
            }

            //合并单元格
            if(isset($value['merge'])){
                $this->excelSheet->mergeCells($key.':'.$value['merge']);
            }
        
            //水平对齐
            if(isset($value['horizontal'])){
                switch ($value['horizontal']) {
                    case 'center':
                        $horizontal = \PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
                        break;
                    
                    case 'left':
                        $horizontal = \PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY;
                        break;
                    case 'right':
                        $horizontal = \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
                        break;
                }
                $column = preg_replace('/\d+/',"",$key);
                $this->excelSheet->getStyle($key)
                    ->getAlignment()
                    ->setHorizontal($horizontal);
                $this->excelSheet->getStyle($column)
                    ->getAlignment()
                    ->setHorizontal($horizontal);
            }

            //垂直对齐
            if(isset($value['vertical'])){
                switch ($value['vertical']) {
                    case 'center':
                        $vertical = \PHPExcel_Style_Alignment::VERTICAL_CENTER;
                        break;
                    
                    case 'top':
                        $vertical = \PHPExcel_Style_Alignment::VERTICAL_TOP;
                        break;
                    case 'bottom':
                        $vertical = \PHPExcel_Style_Alignment::VERTICAL_BOTTOM;
                        break;
                }
                $column = preg_replace('/\d+/',"",$key);
                $this->excelSheet->getStyle($key)
                    ->getAlignment()
                    ->setVertical($vertical);
                $this->excelSheet->getStyle($column)
                    ->getAlignment()
                    ->setVertical($vertical);
            }
        }
        $this->Head = $Head;
        return $this;
    }

    /**
     * 数据导出
     * @return [type] [description]
     */
    public function exportExport(array $data)
    {

        $RowHeight = $this->excelSheet->getRowDimension(1)->getRowHeight();
        foreach ($data as $key => $value) {
            if($this->Config['field_matching']){
                foreach ($value as $k => $v) {
                    $i = 0;
                    foreach ($this->Head as $ii => $j) {
                        if($k == $j['name'] && $this->Config['start_column'] > 0 && $i >= ($this->Config['start_column'] - 1)){
                            $this->excelSheet->setCellValue($this->numbersToLetters($i).($key+$this->Config['data_start']),$v);
                        }
                        $i++;
                    }
                }
            }else{
                $i = 0;
                foreach ($value as $k => $v) {
                    if($this->Config['start_column'] > 0 && $i >= ($this->Config['start_column'] - 1)){
                        $this->excelSheet->setCellValue($this->numbersToLetters($i).($key+$this->Config['data_start']),$v);
                    }
                    $i++;
                }
            }

            $this->excelSheet->getRowDimension($key+$this->Config['data_start'])->setRowHeight($RowHeight);
        }

        switch ($this->Config['writer_type']) {
            case 'Excel2007':
                $suffix = "xlsx";
                break;
            case 'Excel5':
                $suffix = "xls";
                break;
        }
        //工作表名；
        $this->excelSheet->setTitle($this->Config['title']?:$this->Config['file_name']);
        //生成excel文件
        $objWriter = \PHPExcel_IOFactory::createWriter($this->PHPExcel, $this->Config['writer_type']);
        //下载文件在浏览器窗口
        $objWriter->save('php://output');
        
        header("Content-Type: application/force-download,octet-stream,download");  
        header('Content-Disposition:inline;filename="'.$this->Config['file_name'].'.'.$suffix.'"');
        
        exit;
    }

    /**
     * 数字转字母
     * @param  数字
     * @return 字母
     */
    function numbersToLetters($num){
        $ABCstr = "";
        $ten = $num;
        if($ten==0) return "A";
        while($ten!=0){
            if($ten > 0 && $num >= 26 && $ten < 26) $ten -= 1;
            $x = $ten%26;
            $ABCstr .= chr(65+$x);
            $ten = intval($ten/26);
        }
        return strrev($ABCstr);
    }

    /**
     * 字母转数字
     * @param  字母
     * @return 数字
     */
    function toLettersNumbers($abc){
        $ten = 0;
        $len = strlen($abc);
   
        for($i=1;$i<=$len;$i++){
            $char = substr($abc,0-$i,1);//反向获取单个字符
            $int = ord($char);
            $ten += ($int-64)*pow(26,$i-1);
        }
        return $ten-1;
    }
}