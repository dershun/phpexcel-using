# PHPExcel 使用封装

#### 介绍
PHPExcel 导出 Excel表格

适用于、Thinkphp
#### composer 安装

1.  composer require dershun/phpexcel-using

导出示例
~~~

$PhpExcel = new PhpExcelApply();

$excelConfig = [
    "file_name"   => "数据表格"
];

$excelHead = [
    "A1" => [
        "title"      => "数据日期",
        "width"      => 20,
        "height"     => 20,
        "horizontal" => "center",
        "vertical"   => "center",
    ],
];


$PhpExcel->setExportConfig($excelConfig)->setExportHead($excelHead)->exportExport($Data);
~~~
